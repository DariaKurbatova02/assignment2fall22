//Daria Kurbatova
package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("outdoorLiving", 7, 30);
        bikes[1] = new Bicycle("Proffessional bikes", 14, 50);
        bikes[2] = new Bicycle("Bikes for kids", 1, 10);
        bikes[3] = new Bicycle("One wheel bikes", 1, 5);

        for (Bicycle bike : bikes){
            System.out.println(bike);
        }
    }
}
